package id.investree.academyproject.service.user;

import id.investree.academyproject.controller.UserController;
import id.investree.academyproject.entity.User;
import id.investree.academyproject.exception.DataNotFoundException;
import id.investree.academyproject.exception.UnauthorizedException;
import id.investree.academyproject.repository.UserRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public List<User> getAlluser() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User getUser(long userId) {
		
		return userRepository.findById(userId).get();
	}

	@Override
	public User updateUser(long userId, User user) {
		User currUser = userRepository.findById(userId).get();
		if(currUser.getUserIsLogin() == false) throw new UnauthorizedException();
		
		currUser.setUserBio(user.getUserBio());
		currUser.setUserImgPath(user.getUserImgPath());
		currUser.setUserName(user.getUserName());
		
		return userRepository.save(currUser);
	}

	@Override
	public void deleteUser(long user_id) {
		// TODO Auto-generated method stub
	}

	@Override
	public User register(User user) {
		
		final BCryptPasswordEncoder pwEncoder = new BCryptPasswordEncoder();
		user.setUserPassword(pwEncoder.encode(user.getUserPassword()));

		return userRepository.save(user);
	}

	@Override
	public User login(User user) {
		User loggedUser = new User();
		loggedUser = userRepository.findByUserEmail(user.getUserEmail());
		
		final BCryptPasswordEncoder pwEncoder = new BCryptPasswordEncoder();
		
		if(pwEncoder.matches(user.getUserPassword(), loggedUser.getUserPassword())) {
			userRepository.setLogin(true, loggedUser.getUserId());
			return loggedUser;
		} else {
			throw new UnauthorizedException();
		}
	}

	@Override
	public void logout(long userId) {
		User user = userRepository.findById(userId).get();
		if(user.equals(null)) throw new DataNotFoundException();
		if(user.getUserIsLogin() == false) throw new UnauthorizedException();
		
		userRepository.setLogin(false, userId);
	}

	@Override
	public void resetPassword(String userEmail, String newPassword) {
		User user = userRepository.findByUserEmail(userEmail);
		
		if(user.equals(null)) throw new DataNotFoundException();
		if(user.getUserIsLogin() == false) throw new UnauthorizedException(); 
			
		final BCryptPasswordEncoder pwEncoder = new BCryptPasswordEncoder();
		user.setUserPassword(pwEncoder.encode(newPassword));
		userRepository.save(user);
	}

}
