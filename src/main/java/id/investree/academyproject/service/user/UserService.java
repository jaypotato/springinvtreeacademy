package id.investree.academyproject.service.user;

import id.investree.academyproject.entity.User;

import java.util.List;

public interface UserService {
	
	List<User> getAlluser();
	User getUser(long userId);
	User updateUser(long userId, User user);
	void deleteUser(long userId);
	
	User register(User user);
	User login(User user);
	void logout(long userId);
	void resetPassword(String userEmail, String newPassword);
	
}
