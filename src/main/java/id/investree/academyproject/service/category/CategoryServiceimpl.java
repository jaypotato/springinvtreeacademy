package id.investree.academyproject.service.category;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.investree.academyproject.entity.Category;
import id.investree.academyproject.exception.DataNotFoundException;
import id.investree.academyproject.repository.CategoryRepository;

@Service
public class CategoryServiceimpl implements CategoryService {
	
	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public List<Category> getAllCategory() {
		return categoryRepository.findAll();
	}

	@Override
	public Category getCategory(long categoryId) {
		return categoryRepository.findById(categoryId).get();
	}

	@Override
	public Category createCategory(Category category) {
		return categoryRepository.save(category);
	}

	@Override
	public Category updateCategory(long categoryId, Category category) {
		Category currCategory = categoryRepository.findById(categoryId).get();
		if(currCategory.equals(null)) throw new DataNotFoundException();
		category.setCategoryId(categoryId);
		return categoryRepository.save(category);
	}

	@Override
	public void deleteCategory(long categoryId) {
		categoryRepository.deleteById(categoryId);
	}

	@Override
	public List<Category> searchCategory(String query) {
		return categoryRepository.findByCategoryNameLike(query);
	}

}
