package id.investree.academyproject.service.category;

import id.investree.academyproject.entity.Category;

import java.util.List;

public interface CategoryService {

	List<Category> getAllCategory();
	Category getCategory(long categoryId);
	Category createCategory(Category category);
	Category updateCategory(long categoryId, Category category);
	void deleteCategory(long categoryId);
	List<Category> searchCategory(String query);
	
}
