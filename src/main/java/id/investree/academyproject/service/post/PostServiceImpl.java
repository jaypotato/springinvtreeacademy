package id.investree.academyproject.service.post;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.investree.academyproject.entity.Post;
import id.investree.academyproject.exception.DataNotFoundException;
import id.investree.academyproject.repository.PostRepository;

@Service
public class PostServiceImpl implements PostService {

	@Autowired
	private PostRepository postRepository;
	
	@Override
	public List<Post> getAllPost() {
		return postRepository.findAll();
	}

	@Override
	public Post getPost(long postId) {
		return postRepository.findById(postId).get();
	}

	@Override
	public Post createPost(Post post) {
		return postRepository.save(post);
	}

	@Override
	public Post updatePost(long postId, Post post) {
		Post currPost = postRepository.findById(postId).get();
		if(currPost.equals(null)) throw new DataNotFoundException();
		
		post.setPostId(postId);
		return postRepository.save(post);
	}

	@Override
	public void deletePost(long postId) {
		postRepository.deleteById(postId);
	}

	@Override
	public List<Post> searchPost(String query) {
		return postRepository.findByPostTitleLike(query);
	}

}
