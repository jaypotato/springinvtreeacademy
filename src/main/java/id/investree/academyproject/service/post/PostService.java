package id.investree.academyproject.service.post;

import id.investree.academyproject.entity.Post;

import java.util.List;

public interface PostService {

	List<Post> getAllPost();
	Post getPost(long postId);
	Post createPost(Post post);
	Post updatePost(long postId, Post post);
	void deletePost(long postId);
	List<Post> searchPost(String query);
	
}
