package id.investree.academyproject.repository;

import id.investree.academyproject.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

import javax.transaction.Transactional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

	@Transactional
	@Modifying
	@Query(value = "update user u set u.user_is_login = ?1 where u.user_id = ?2", nativeQuery = true)
	public void setLogin(boolean b, long userId);

	@Query(value = "select * from user where user_email = ?1", nativeQuery = true)
	public User findByUserEmail(String userEmail);
	
}
