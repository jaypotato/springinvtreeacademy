package id.investree.academyproject.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import id.investree.academyproject.entity.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long>{

	@Query(value = "select * from post where post_title like %?1%", nativeQuery = true)
	List<Post> findByPostTitleLike(String query);

}
