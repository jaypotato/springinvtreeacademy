package id.investree.academyproject.exception;

import org.springframework.dao.DataIntegrityViolationException;

public class DataDuplicateException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public DataDuplicateException(Exception e) {
		super(e);
	}

	public DataDuplicateException(DataIntegrityViolationException e) {
		super(e);
	}
	
}
