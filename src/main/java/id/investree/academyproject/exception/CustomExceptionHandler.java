package id.investree.academyproject.exception;

import id.investree.academyproject.model.Response;
import id.investree.academyproject.model.MetaResponse;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(DataDuplicateException.class)
	public ResponseEntity handleDuplicate(DataDuplicateException e) {
		String message = "Email has been taken";
		Response response = new Response();
		response.setStatus(response.ERROR);
		response.setMetaResponse(new MetaResponse(HttpStatus.CONFLICT.value(), message, e.getMessage()));
		
		return new ResponseEntity(response, HttpStatus.CONFLICT);
	}
	
	@ExceptionHandler(UnauthorizedException.class)
	public ResponseEntity handleUnauthorized(UnauthorizedException e) {
		String message = "Unauthorized, You are not logged in";
		Response response = new Response();
		response.setStatus(response.ERROR);
		response.setMetaResponse(new MetaResponse(HttpStatus.UNAUTHORIZED.value(), message, e.getMessage()));
		
		return new ResponseEntity(response, HttpStatus.UNAUTHORIZED);
	}
	
	@ExceptionHandler(DataCreationException.class)
	public ResponseEntity handleDataCreationException(DataCreationException e) {
		String message = "Something when trying to create data";
		Response response = new Response();
		response.setStatus(response.ERROR);
		response.setMetaResponse(new MetaResponse(HttpStatus.INTERNAL_SERVER_ERROR.value(), message, e.getMessage()));
		
		return new ResponseEntity(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(DataNotFoundException.class)
	public ResponseEntity handleDataNotFoundException(DataNotFoundException e) {
		String message = "Data not found";
		Response response = new Response();
		response.setStatus(response.ERROR);
		response.setMetaResponse(new MetaResponse(HttpStatus.NOT_FOUND.value(), message, e.getMessage()));
		
		return new ResponseEntity(response, HttpStatus.NOT_FOUND);
	}
}
