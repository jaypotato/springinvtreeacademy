package id.investree.academyproject.exception;

public class DataCreationException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public DataCreationException(Exception e) {
		super(e);
	}
	
	public DataCreationException() {

	}
}
