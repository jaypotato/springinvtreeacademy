package id.investree.academyproject.exception;

public class DataNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	public DataNotFoundException(Exception e) {
		super(e);
	}
	
	public DataNotFoundException() {

	}
}
