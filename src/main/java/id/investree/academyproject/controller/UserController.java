package id.investree.academyproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.investree.academyproject.service.user.UserService;
import id.investree.academyproject.entity.User;
import id.investree.academyproject.exception.DataDuplicateException;
import id.investree.academyproject.exception.DataNotFoundException;
import id.investree.academyproject.exception.UnauthorizedException;
import id.investree.academyproject.model.Response;
import id.investree.academyproject.model.MetaResponse;

@RestController
@RequestMapping("/user")
public class UserController {
	
	private UserService userService;
	
	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}
	
//	@GetMapping("/all")
//	public List<User> getAllUser() {
//		return userService.getAlluser();
//	}
	
	@PostMapping("/register")
	public Response register(@RequestBody User user) {
		try {
			Response response  = new Response(Response.OK, userService.register(user), 
					new MetaResponse(HttpStatus.OK.value(), "Registration Success", null));
			return response;
			
		} catch(DataIntegrityViolationException e) {
			throw new DataDuplicateException(e);
		}		
	}
	
	@PostMapping("/login")
	public Response login(@RequestBody User user) {
		try {
			Response response  = new Response(Response.OK, userService.login(user),
					new MetaResponse(HttpStatus.OK.value(), "Login Successfull", null));
			return response;
			
		} catch(Exception e) {
			throw new UnauthorizedException(e);
		}
	}
	
	@PostMapping("/reset-password")
	public Response resetPassword(@RequestBody User user) {
		try {
			userService.resetPassword(user.getUserEmail(), user.getUserPassword());
			Response response = new Response(Response.OK, "",
					new MetaResponse(HttpStatus.OK.value(), "Password Reset Successfully", null));
			return response;
		} catch(Exception e) {
			if(e.getClass().equals(DataNotFoundException.class))
				throw new DataNotFoundException(e);
			else throw new UnauthorizedException(e);
		}
	}
	
	@PostMapping("/profile/edit")
	public Response profileEdit(@RequestBody User user) {
		try {
			Response response = new Response(Response.OK, userService.updateUser(user.getUserId(), user), 
					new MetaResponse(HttpStatus.OK.value(), "Profile edited", null));
			return response;
		} catch(Exception e) {
			throw new UnauthorizedException(e);
		}
	}
	
	@GetMapping("/profile/view/{id}")
	public Response profileView(@PathVariable(name = "id") long userId) {
		try {
			Response response = new Response(Response.OK, userService.getUser(userId),
					new MetaResponse(HttpStatus.OK.value(), "User found", null));
			return response;
		} catch(Exception e) {
			throw new DataNotFoundException();
		}
	}
	
	@PostMapping("/logout")
	public Response logout(@RequestBody User user) {
		try {
			userService.logout(user.getUserId());
			Response response = new Response(Response.OK, "", 
					new MetaResponse(HttpStatus.OK.value(), "Logged out", null));
			return response;
		} catch(Exception e) {
			if(e.getClass().equals(DataNotFoundException.class)) throw new DataNotFoundException(e);
			else throw new UnauthorizedException(e);
		}
	}
}
