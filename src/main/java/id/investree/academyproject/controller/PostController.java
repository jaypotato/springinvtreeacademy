package id.investree.academyproject.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.investree.academyproject.model.Response;
import id.investree.academyproject.entity.Post;
import id.investree.academyproject.exception.DataCreationException;
import id.investree.academyproject.exception.DataNotFoundException;
import id.investree.academyproject.model.MetaResponse;
import id.investree.academyproject.service.post.PostService;


@RestController
@RequestMapping("/post")
public class PostController {

	private PostService postService;
	
	@Autowired
	public PostController(PostService postService) {
		this.postService = postService;
	}
	
	@GetMapping("/all")
	public Response getAllPost() {
		Response response = new Response(Response.OK, postService.getAllPost(),
				new MetaResponse(HttpStatus.OK.value(), "Get all post", null));
		return response;
	}
	
	@PostMapping("/add")
	public Response addPost(@RequestBody Post post) {
		try {
			Response response = new Response(Response.OK, postService.createPost(post),
					new MetaResponse(HttpStatus.OK.value(), "Post created", null));
			return response;
			
		} catch(Exception e) {
			throw new DataCreationException(e);
		}
	}
	
	@GetMapping("/{id}")
	public Response getPostById(@PathVariable(name = "id") long postId) {
		try {
			Response response = new Response(Response.OK, postService.getPost(postId),
					new MetaResponse(HttpStatus.OK.value(), "Post found", null));
			return response;
			
		} catch(Exception e) {
			throw new DataNotFoundException(e);
		}
	}
	
	@GetMapping("/edit/{id}")
	public Response updatePost(@PathVariable(name = "id") long postId, @RequestBody Post post) {
		try {
			Response response = new Response(Response.OK, postService.updatePost(postId, post),
					new MetaResponse(HttpStatus.OK.value(), "Post edited", null));
			return response;
			
		} catch(Exception e) {
			throw new DataNotFoundException(e);
		}
	}
	
	@GetMapping("/delete/{id}")
	public Response deletePost(@PathVariable(name = "id") long postId) {
		try {
			postService.deletePost(postId);
			Response response = new Response(Response.OK,"",
					new MetaResponse(HttpStatus.OK.value(), "Post deleted", null));
			return response;
			
		} catch(Exception e) {
			throw new DataNotFoundException(e);
		}
	}
	
	@GetMapping("/search")
	public Response searchPost(@RequestParam(name = "query") String query) {
		Response response = new Response(Response.OK, postService.searchPost(query),
				new MetaResponse(HttpStatus.OK.value(), "Showing search result", null));
		return response;
	}
}
