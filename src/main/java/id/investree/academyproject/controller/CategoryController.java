package id.investree.academyproject.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import id.investree.academyproject.entity.Category;
import id.investree.academyproject.entity.User;
import id.investree.academyproject.exception.DataCreationException;
import id.investree.academyproject.exception.DataNotFoundException;
import id.investree.academyproject.model.MetaResponse;
import id.investree.academyproject.model.Response;
import id.investree.academyproject.service.category.CategoryService;

@RestController
@RequestMapping("/category")
public class CategoryController {
	
	private CategoryService categoryService;
	
	@Autowired
	public CategoryController(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	
	@GetMapping("/all")
	public Response getAllCategory() {
		Response response = new Response(Response.OK, categoryService.getAllCategory(),
				new MetaResponse(HttpStatus.OK.value(), "Get all category", null));
		return response;
	}
	
	@PostMapping("/add")
	public Response addCategory(@RequestBody Category category) {
		try {
			Response response = new Response(Response.OK, categoryService.createCategory(category),
					new MetaResponse(HttpStatus.OK.value(), "Category Created", null));
			return response;
			
		} catch(Exception e) {
			throw new DataCreationException(e);
		}
	}
	
	@GetMapping("/{id}")
	public Response getCategoryById(@PathVariable(name = "id") long categoryId) {
		try {
			Response response = new Response(Response.OK, categoryService.getCategory(categoryId),
					new MetaResponse(HttpStatus.OK.value(), "Category found", null));
			return response;
			
		} catch(Exception e) {
			throw new DataNotFoundException(e);
		}
	}
	
	@GetMapping("/edit/{id}")
	public Response updateCategory(@PathVariable(name = "id") long categoryId, @RequestBody Category category) {
		try {
			Response response = new Response(Response.OK, categoryService.updateCategory(categoryId, category),
					new MetaResponse(HttpStatus.OK.value(), "Category edited", null));
			return response;
			
		} catch(Exception e) {
			throw new DataNotFoundException(e);
		}
	}
	
	@GetMapping("/delete/{id}")
	public Response deleteCategory(@PathVariable(name = "id") long categoryId) {
		try {
			categoryService.deleteCategory(categoryId);
			Response response = new Response(Response.OK, "",
					new MetaResponse(HttpStatus.OK.value(), "Category deleted", null));
			return response;
			
		} catch(Exception e) {
			throw new DataNotFoundException(e);
		}
	}
	
	@GetMapping("/search")
	public Response searchCategory(@RequestParam(name = "query") String query) {
		Response response = new Response(Response.OK, categoryService.searchCategory(query),
				new MetaResponse(HttpStatus.OK.value(), "Showing search result", null));
		return response;
	}
}
