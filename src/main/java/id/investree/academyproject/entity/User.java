package id.investree.academyproject.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long userId;
	
	@Column(name = "user_name", unique = false, nullable = false)
	private String userName;
	
	@Column(name = "user_email", unique = true, nullable = false)
	private String userEmail;
	
	@Column(name = "user_img_path", unique = false, nullable = true)
	private String userImgPath;
	
	@Column(name = "user_bio", unique = false, nullable = true)
	private String userBio;
	
	@Column(name = "user_password", unique = false, nullable = false)
	private String userPassword;
	
	@Column(name = "user_is_login", unique = false, nullable = true)
	private Boolean userIsLogin;
	
	@CreationTimestamp
	@Column(name = "created_at", nullable = false, updatable = false)
	private Timestamp createdAt;
	
	@UpdateTimestamp
	@Column(name = "updated_at")
	private Timestamp updatedAt;
	
	public User() {
	
	}
	
	public User(long userId, String userName, String userEmail, String userImgPath, String userBio,
			String userPassword) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.userEmail = userEmail;
		this.userImgPath = userImgPath;
		this.userBio = userBio;
		this.userPassword = userPassword;
	}


	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserImgPath() {
		return userImgPath;
	}

	public void setUserImgPath(String userImgPath) {
		this.userImgPath = userImgPath;
	}

	public String getUserBio() {
		return userBio;
	}

	public void setUserBio(String userBio) {
		this.userBio = userBio;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getUserPassword() {
		return userPassword;
	}

	public Boolean getUserIsLogin() {
		return userIsLogin;
	}

	public void setUserIsLogin(Boolean userIsLogin) {
		this.userIsLogin = userIsLogin;
	}
		
}
