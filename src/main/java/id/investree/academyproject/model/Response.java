package id.investree.academyproject.model;

public class Response {

	public static final String OK = "OK";
	public static final String ERROR = "ERROR";
	
	private String status;
	private Object object;
	private MetaResponse metaResponse;
	
	public Response() {
		
	}
	
	public Response(String status, Object object, MetaResponse metaResponse) {
		super();
		this.status = status;
		this.object = object;
		this.metaResponse = metaResponse;
	}
	
	public Response(String status, MetaResponse metaResponse) {
		this.status = status;
		this.metaResponse = metaResponse;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}

	public MetaResponse getMetaResponse() {
		return metaResponse;
	}

	public void setMetaResponse(MetaResponse metaResponse) {
		this.metaResponse = metaResponse;
	}
	
	
}
